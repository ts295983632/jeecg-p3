package com.jeecg.teamrank.main.dao;

import java.util.List;

import org.jeecgframework.minidao.annotation.Param;
import org.jeecgframework.minidao.annotation.ResultType;
import org.jeecgframework.minidao.annotation.Sql;
import org.jeecgframework.minidao.pojo.MiniDaoPage;
import org.springframework.stereotype.Repository;

import com.jeecg.teamrank.main.entity.JpTeamPerson;


/**
 * 描述：</b>JpTeamPersonDao<br>
 * @author：p3.jeecg
 * @since：2016年07月14日 10时10分49秒 星期四 
 * @version:1.0
 */
@Repository
public interface JpTeamPersonDao{

    /**
	 * 查询返回Java对象
	 * @param id
	 * @return
	 */
	@Sql("SELECT * FROM jp_team_person WHERE ID = :id")
	JpTeamPerson get(@Param("id") String id);
	
	/**
	 * 修改数据
	 * @param jpTeamPerson
	 * @return
	 */
	int update(@Param("jpTeamPerson") JpTeamPerson jpTeamPerson);
	
	/**
	 * 插入数据
	 * @param act
	 */
	void insert(@Param("jpTeamPerson") JpTeamPerson jpTeamPerson);
	
	/**
	 * 通用分页方法，支持（oracle、mysql、SqlServer、postgresql）
	 * @param jpTeamPerson
	 * @param page
	 * @param rows
	 * @return
	 */
	@ResultType(JpTeamPerson.class)
	public MiniDaoPage<JpTeamPerson> getAll(@Param("jpTeamPerson") JpTeamPerson jpTeamPerson,@Param("page")  int page,@Param("rows") int rows);
	
	@Sql("DELETE from jp_team_person WHERE ID = :jpTeamPerson.id")
	public void delete(@Param("jpTeamPerson") JpTeamPerson jpTeamPerson);
	
	
	@Sql("select * from jp_team_person ORDER BY is_join DESC")
	public List<JpTeamPerson> findByQueryString();
	
	@Sql("select * from jp_team_person where id=:id")
	public JpTeamPerson findByQuery(@Param("id") String id);
	
}

