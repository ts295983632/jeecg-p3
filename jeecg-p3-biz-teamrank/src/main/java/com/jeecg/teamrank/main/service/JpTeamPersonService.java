package com.jeecg.teamrank.main.service;

import java.util.List;

import org.jeecgframework.minidao.annotation.Param;
import org.jeecgframework.minidao.pojo.MiniDaoPage;
import com.jeecg.teamrank.main.entity.JpTeamPerson;

/**
 * 描述：</b>JpTeamPersonService<br>
 * @author:p3.jeecg
 * @since：2016年07月14日 10时10分49秒 星期四 
 * @version:1.0
 */
public interface JpTeamPersonService {
	public JpTeamPerson get(String id);

	public int update(JpTeamPerson jpTeamPerson);

	public void insert(JpTeamPerson jpTeamPerson);

	public MiniDaoPage<JpTeamPerson> getAll(JpTeamPerson jpTeamPerson,int page,int rows);

	public void delete(JpTeamPerson jpTeamPerson);

	public List<JpTeamPerson> findByQueryString();

	public JpTeamPerson findByQuery(String id);
	
}
